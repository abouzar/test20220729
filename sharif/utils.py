def convert_to_persian_numbers(text: str):
    dictionary = {"1": "۱", "2": "۲", "3": "۳", "4": "۴", "5": "۵", "6": "۶", "7": "۷", "8": "۸", "9": "۹", "0": "۰"}
    result = text
    for key, value in dictionary.items():
        result = result.replace(key, value)
    return result
